What's the purpose of all this? Great question!

* I want to learn how to use Bitbucket, and by extension, other Atlassian tools. 
* I also need to re-familiarize myself with Git. It's been a while since I've used it regularly; for a while now, I've just been using SVN, mainly because it's what is used at work.
* Of course, I want to create a Twitter bot based on my own personal account. Along the way, I'll learn some Ruby basics and create a Heroku worker so that the bot will be running all the time.

Resources that I've referenced for creating the Printer Ebooks bot:
For making the bot in Ruby: http://sts10.github.io/blog/2014/12/23/guide-create-markov-twitter-bot/
For running the bot on Heroku: http://jenniferkruse.me/twitterbot.html